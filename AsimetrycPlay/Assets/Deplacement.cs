﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deplacement : MonoBehaviour {

    public float speed = 100.0f;
    public Animator animation;


    private bool facingRight;
    private bool timeToJump;

    // Use this for initialization
    void Start () {
        facingRight = true;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        float horizontal = Input.GetAxis("Horizontal");
        float translation = horizontal * speed;

        translation *= Time.fixedDeltaTime;
        transform.Translate(translation, 0, 0);
        Flip(horizontal);
        jump();
        
        
    }

    private void Update()
    {
        if (Input.GetButtonDown("Jump") && Grounded())
        {
            animation.SetBool("isWalk", false);
            animation.SetBool("isIdle", false);
            animation.SetBool("isJump", true);
            timeToJump = true;
            //transform.GetComponent<Rigidbody2D>().AddForce(transform.up * 4000 * Time.fixedDeltaTime);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            animation.SetBool("isJump", false);
            animation.SetBool("isIdle", false);
            animation.SetBool("isWalk", true);
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            animation.SetBool("isJump", false);
            animation.SetBool("isIdle", false);
            animation.SetBool("isWalk", true);
        }
        else
        {
            animation.SetBool("isWalk", false);
            animation.SetBool("isJump", false);
            animation.SetBool("isIdle", true);
        }
    }

    private void jump()
    {
        if (timeToJump)
        {
            transform.GetComponent<Rigidbody2D>().AddForce(transform.up * 4000 * Time.fixedDeltaTime);
            timeToJump = false;
        }
    }

    private void Flip(float horizontal)
    {
        if (horizontal > 0 && !facingRight || horizontal < 0 && facingRight)
        {
            facingRight = !facingRight;

            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }
    }

    private bool Grounded()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.up);
        if (hit.collider != null)
        {
            float distance = Mathf.Abs(hit.point.y - transform.position.y);
            if (distance <=0.1f)
            {
                Debug.Log("grounded");
                return true;
            }
        }
        return false;
    }

}
